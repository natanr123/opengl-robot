/**
 * Open University - Course: Computer Graphics 20562
 * Semester 2017 B, MMN 17
 * Created By:
 * Natan Rubinstein 066511353
 * Ariel Pinchover 203437587
 * 
 */

package model;

public class Consts {
	
	public static int VIEW_3P = 0;
	public static int VIEW_1P = 1;
	public static int VIEW_SPOT_LIGHT = 3;
	public static Vector3D ROBOT_START_POSTION = new Vector3D(1.0f, 0.0f, 0.0f);
	public static float _180_OVER_PI = 57.29577951308232087665461840231273527024f;
	
}
